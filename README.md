# Rasphome

### Prepare raspberry pi

#### install mqqt broker mosquitto and python wrapper for mqtt
```
sudo apt-get install mosquitto mosquitto-clients python-pip git

pip install paho-mqtt
```

#### install node-red
```
bash <(curl -sL https://raw.githubusercontent.com/node-red/raspbian-deb-package/master/resources/update-nodejs-and-nodered)
```