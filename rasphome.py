import os
import time
import sys
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

import subprocess

import RPi.GPIO as GPIO

import Adafruit_DHT

SERVER_ADRESS = '192.168.1.39'
# ACCESS_TOKEN = 'TOKEN'

class Config():
    def __init__(self):
         self.refreshTimeSec = 5

class Data():
    def __init__(self):
         self.status = 1
         self.temperatureGoal = 20
         self.sensorTemperature = 20
         self.heating = False
         self.led = False


def on_message(client, userdata, message):
    content = str(message.payload.decode("utf-8"))
    #print("message received " ,content)
    #print("message topic=",message.topic)
    #print("message qos=",message.qos)
    #print("message retain flag=",message.retain)
    print(" ")
    rasphome.onInputMessage(message.topic[len("cmd/"):], content)

class IO():
    def __init__(self):
        self.pinLed = 4
        self.pinHeater = 17
        #self.pinSensorTemp = 17
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pinLed, GPIO.OUT, initial = GPIO.HIGH)
        GPIO.setup(self.pinHeater, GPIO.OUT, initial = GPIO.LOW)
        #GPIO.setup(self.pinSensorTemp, GPIO.IN)

    def setHeating(self, active):
        state = GPIO.LOW
        if active is True :
            state = GPIO.HIGH
        GPIO.output(self.pinHeater, state)

    def setLed(self, active):
        state = GPIO.LOW
        if active is True :
            state = GPIO.HIGH
        GPIO.output(self.pinLed, state)

    def getTemperature(self):
        return 55
        humidity, temperature = Adafruit_DHT.read_retry(Adafruit_DHT.DHT11,  self.pinSensorTemp)
        if humidity is not None and temperature is not None:
            print('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(temperature, humidity))
            return temperature
        print('Failed to get temperature!')
        return -100


class RaspHome():
    def __init__(self):
        #create data
        self.config = Config()
        self.data = Data()
        self.io = IO()
        self.runnig = False
        self.client = mqtt.Client("rasp")
        self.client.on_message = on_message
        self.client.connect(SERVER_ADRESS, 1883, 60)
        self.client.loop_start()
        self.client.subscribe("cmd/#")

    def callCmd(self,cmd, arg):
        return subprocess.Popen([cmd, arg])
        #subprocess.Popen([cmd, arg],creationflags=DETACHED_PROCESS, shell = True)

    def onInputMessage(self, topic, content):
        print ("receive message " + topic + " : " + content)
        if (topic == "temperature"):
            self.data.temperatureGoal = int(content)
        if (topic == "heating"):
            self.io.setHeating(content == "1")
        if (topic == "led"):
            self.io.setLed(content == "1")
        if (topic == "radio"):
            if content == "1" and self.paudio == None:
                print("launching radio")
                self.paudio = self.callCmd("mplayer", "http://direct.franceinter.fr/live/franceinter-midfi.mp3")
            else:
                if self.paudio is not None:
                    print("try to kill mplayer process")
                    self.paudio.terminate()
                    self.paudio = None

    #load all plugins and run the main loop
    def run(self):
        self.runnig = True
        print("launching rasphome")
        self.publishStart()
        
        try:
            while self.runnig:
                self.publish()
                #self.client.loop()
                time.sleep(self.config.refreshTimeSec)
        except KeyboardInterrupt:
            self.runnig = False

        print("quitting rasphome")
        self.publishStart()
        time.sleep(1)

        self.client.publish("status", "0")
        self.client.loop_stop()
        self.client.disconnect()

    #stop the main loop    
    def stop(self):
        self.runnig = False

    def publishStart(self):
        self.client.publish("cmd/heating", "0")
        self.client.publish("cmd/led", "0")
        self.client.publish("cmd/radio", "0")
        self.client.publish("temperature", "20")

    def publish(self):
        self.client.publish("status", "1")
        self.client.publish("temperature", self.data.temperatureGoal)
        #temp = self.io.getTemperature()
        #self.client.publish("temperature", str(temp))

# main() : run the loop
rasphome = RaspHome()
try:
    rasphome.run()
except KeyboardInterrupt:
    print("\nquit loop")
